import java.util.Scanner;

public class E12 {

	//Write a program in Java to display the n terms of odd natural
	//number and their sum.
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner in= new Scanner (System.in);
		System.out.println("Va rog inserati numarul de numere impare:");
		int nrNrImpare=in.nextInt();
			
		int i;
		int sumaNr;
		sumaNr=0;
		String sirNrImpare;
		sirNrImpare="";
				
		for(i=1 ; i<=(2*nrNrImpare) ; i=i+2) {
			
			
			sirNrImpare=sirNrImpare + i + ", ";
			sumaNr=	sumaNr + i;
			
		}
		
		System.out.println("Numerele impare sunt:" + sirNrImpare);
		System.out.println("Suma numerelor impare este:" + sumaNr);
	}

}
