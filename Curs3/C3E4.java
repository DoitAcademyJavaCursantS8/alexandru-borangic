import java.util.Scanner;

public class C3E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
/*Create the X and 0 game.
� Read from the keyboard the positions where to put the
values of each player.
 E.g.
� Put X for row = 0 and col = 2
� Put 0 for row = 1 and col = 1
� Check if it is valid the position where the player wants to
insert a value. If not then the player should retry to add a
value.
� Show a message when the game is over to indicate the
winner.*/
		
		String[][] matriceX = new String[3][3];
		String[][] matrice0 = new String[3][3];
		int rand,coloana;
		Scanner scan=new Scanner(System.in);
		loop1:
		for (int nrMutari=0;nrMutari<9;nrMutari++) {
			//introduce mutarile lui X
			System.out.println("Put X for row= ");
			rand=scan.nextInt();	
			
			
			if((rand<0)||(rand>2)) {
				System.out.println("Va rog introduceti un rand intre 0 si 2 pentru X!");
				rand=scan.nextInt();}
			
			System.out.print(" and column= ");
			scan=new Scanner(System.in);
			coloana=scan.nextInt();
			if((coloana<0)||(coloana>2)) {
				System.out.println("Va rog introduceti o coloana intre 0 si 2 pentru X!");
				coloana=scan.nextInt();
			}
			
			matriceX[rand][coloana]="X";
			
			//introduce mutarile lui 0
			
			System.out.println("Put 0 for row= ");
			rand=scan.nextInt();	
			
			
			if((rand<0)||(rand>2)) {
				System.out.println("Va rog introduceti un rand intre 0 si 2pentru 0!");
				rand=scan.nextInt();}
			
			System.out.print(" and column= ");
			scan=new Scanner(System.in);
			coloana=scan.nextInt();
			if((coloana<0)||(coloana>2)) {
				System.out.println("Va rog introduceti o coloana intre 0 si 2 pentru 0!");
				coloana=scan.nextInt();}
			
			
			matrice0[rand][coloana]="0";
			
			if (nrMutari>=2) {
				//verifica pe linie si pe coloana
				for (int i=0;i<3;i++) {
					
				if(matriceX[i][0]=="X" && matriceX[i] [1]=="X" && matriceX[i] [2]=="X") {
					System.out.println("X player WIN!");
					break loop1;}
					
				if(matriceX[0][i]=="X" && matriceX[1] [i]=="X" && matriceX[2] [i]=="X") {
					System.out.println("X player WIN!");
					break loop1;}
				
				if(matriceX[i][0]=="0" && matriceX[i] [1]=="0" && matriceX[i] [2]=="0") {
					System.out.println("0 player WIN!");
					break loop1;}
					
				if(matriceX[0][i]=="0" && matriceX[1] [i]=="0" && matriceX[2] [i]=="0") {
					System.out.println("0 player WIN!");
					break loop1;}
				
				}
				
				//verifica pe diagonala
				if(matriceX[0][0]=="X" && matriceX[1] [1]=="X" && matriceX[2] [2]=="X") {
					System.out.println("X player WIN!");
					break loop1;}
				if(matriceX[0][0]=="0" && matriceX[1] [1]=="0" && matriceX[2] [2]=="0") {
					System.out.println("0 player WIN!");
					break loop1;}
				if(matriceX[2][2]=="X" && matriceX[1] [1]=="X" && matriceX[0] [0]=="X") {
					System.out.println("X player WIN!");
					break loop1;}
				if(matriceX[2][2]=="0" && matriceX[1] [1]=="0" && matriceX[0] [0]=="0") {
					System.out.println("0 player WIN!");
					break loop1;}
				
			}
		}
		
		System.out.println("Remiza!");
	}

}
