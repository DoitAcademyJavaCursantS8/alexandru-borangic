import java.util.Scanner;

public class C3E3_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//multiply by a Constant
		
		int randuri,coloane,constanta;
		Scanner scan=new Scanner(System.in);
		
		System.out.println("Introduceti numarul de randuri:");
		randuri=scan.nextInt();
		System.out.println("Introduceti numarul de coloane:");
		scan=new Scanner(System.in);
		coloane=scan.nextInt();
		
		int[][] matrice=new int[randuri][coloane];
		int[][] matriceRezultat=new int[randuri][coloane];
		
		System.out.println("Introduceti constanta cu care trebuie inmultita matricea:");
		scan= new Scanner(System.in);
		constanta=scan.nextInt();
		
		for(int i=0;i<randuri;i++) {
			for (int j=0;j<coloane;j++) {
				System.out.println("Introduceti numarul de pe randul "+(i+1)+" si coloana "+(j+1));
				scan=new Scanner (System.in);
				matrice[i][j]=scan.nextInt();
			}
		}
		
		System.out.println("Rezultatul inmultirii matricii cu constanta: "+constanta+" este:");
		System.out.println();
		for(int i=0;i<randuri;i++) {
			for (int j=0;j<coloane;j++) {
				
				matriceRezultat[i][j]=constanta*matrice[i][j];
				System.out.print(matriceRezultat[i][j]+" ");
			}
			System.out.println();
		}
	}

}
