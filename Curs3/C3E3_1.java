import java.util.Scanner;

public class C3E3_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//add two matrices: add the numbers in the matching positions
		
		int randuri,coloane,i,j;
		
		Scanner scan=new Scanner(System.in);
		
		System.out.println("Introduceti numarul de randuri");
		 randuri=scan.nextInt();
		
		 System.out.println("Introduceti numarul de coloane:");
		 scan=new Scanner(System.in);
		 coloane=scan.nextInt();
		 
		 int[][] matrice1=new int[randuri][coloane];
		 int[][] matrice2=new int[randuri][coloane];
		 int[][] sumaMatrici=new int[randuri][coloane];
		 
		 for(i=0;i<randuri;i++) {
			 for(j=0;j<coloane;j++) {
				 
				 System.out.println("Va rog sa introduceti valoarea pentru matricea 1, pozitia " +(i+1)+" "+(j+1));
				 scan=new Scanner(System.in);
				 matrice1 [i][j] =scan.nextInt();
				 
				 
			 }
		 }
		 
		 for(i=0;i<randuri;i++) {
			 for(j=0;j<coloane;j++) {
				 
				 System.out.println("Va rog sa introduceti valoarea pentru matricea 2, pozitia "+(i+1)+" "+(j+1));
				 scan=new Scanner(System.in);
				 matrice2 [i][j] =scan.nextInt();
				 
				 
			 }
		 }
		 
		 for(i=0;i<randuri;i++) {
			 for(j=0;j<coloane;j++) {
				  sumaMatrici[i][j]=matrice1 [i][j]+matrice2 [i][j];
				 
				 
			 }
		 }
		 
		 for(i=0;i<randuri;i++) {
			 for(j=0;j<coloane;j++) {
				  System.out.print(sumaMatrici[i][j]+" ");
				  
			 }
			 System.out.println();
			 
		 }
		 
	}

}
