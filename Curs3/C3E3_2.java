import java.util.Scanner;

public class C3E3_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//negative of a matrix
		
		int randuri,coloane;
		
		System.out.println("Va rog sa introduceti numarul de randuri:");
		Scanner scan = new Scanner(System.in);
		randuri=scan.nextInt();
		
		System.out.println("Va rog sa introduceti numarul de coloane: ");
		scan=new Scanner(System.in);
		coloane=scan.nextInt();
		
		int[][] matrice=new int[randuri][coloane];
		
		for (int i=0; i<randuri;i++) {
			for(int j=0;j<coloane;j++) {
				scan=new Scanner(System.in);
				System.out.println("Va rog sa introduceti numarul de pe randul: "+(i+1)+" si coloana:"+(j+1));
				matrice[i][j]=scan.nextInt();
								
			}
		}
		
		//facem negativul matricii
		for (int i=0; i<randuri;i++) {
			for(int j=0;j<coloane;j++) {
				matrice[i][j]=-matrice[i][j];
								
			}
		}
		
		//afisam matricea
		for (int i=0; i<randuri;i++) {
			for(int j=0;j<coloane;j++) {
				System.out.print(matrice[i][j]+" ");
								
			}
			System.out.println();
		}
		
	}

}
