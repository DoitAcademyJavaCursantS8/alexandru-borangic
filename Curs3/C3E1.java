import java.util.Scanner;

public class C3E1 {

	/*
	 Write an input program where are counted the number of people
registered using their name.
The registration is finished when it is received as input the secret
command "EXIT".
Show the summary of all registered persons.
Please tell me your name: John
Hello John, please enter you age: 24
Thank you John for your registration!
Please tell me your name: Bob
Hello Bob, please enter you age: 31
Thank you Bob for your registration!
Please tell me your name: EXIT
Below you can find a list with 2 registered persons:
Name Age
John 24
Bob 31 
	 */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		
		String[][] matrice=new String[100][2];
		
		int i,j;
		j=0;
		String nume;
		String varsta;
		
		loop1:
		for (i=0 ; i>=0 ; i++) {
			
			
			Scanner scan=new Scanner(System.in);
			
			System.out.println("Please tell me your name: ");
			nume= scan.nextLine();
			
			 if (nume.equals("EXIT")){ break loop1;}
			 
			 System.out.println("Hello "+ nume +",please tell me your age: ");
			 scan=new Scanner(System.in);
			 varsta= scan.nextLine();
			 
			 matrice[i][0]=nume;
			 matrice[i][1]=varsta;
			 
			 System.out.println("Thank you "+ matrice[i][0] + " for your registration!");
			 
			 
			 j=j+1; 
		}
		
		System.out.println("Name   "+"Age");
		
		 for (i=0; i<j;i++ ) {
			 System.out.println(matrice[i][0]+"  "+matrice[i][1]);
		 }
		
	}

}
