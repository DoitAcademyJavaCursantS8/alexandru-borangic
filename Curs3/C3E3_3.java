import java.util.Scanner;

public class C3E3_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//subtract two matrices
		
		int randuri,coloane;
		
		System.out.println("Introduceti numarul de randuri: ");
		Scanner scan=new Scanner(System.in);
		randuri=scan.nextInt();
		
		System.out.println("Introduceti numarul de coloane: ");
		scan=new Scanner(System.in);
		coloane= scan.nextInt();
		
		int[][] matrice1=new int[randuri][coloane];
		int[][] matrice2=new int[randuri][coloane];
		int[][] matriceRezultat=new int[randuri][coloane];
		
		//introduc elementele primei matrici
		for (int i=0;i<randuri;i++) {
			for (int j=0;j<coloane;j++) {
				System.out.println("Introduceti elementul Matricii1 de pe randul"+(i+1)+" si coloana"
						+(j+1));
				scan=new Scanner(System.in);
				matrice1[i][j]=scan.nextInt();
				
			}
		}
		
		//introduc elementele celei de a doua matrici
				for (int i=0;i<randuri;i++) {
					for (int j=0;j<coloane;j++) {
						System.out.println("Introduceti elementul Matricii 2 de pe randul"+(i+1)+" si coloana"
								+(j+1));
						scan=new Scanner(System.in);
						matrice2[i][j]=scan.nextInt();
						
					}
				}
				
		//fac scaderea matricelor
				System.out.println("Rezultatul scaderii elementelor Matricii2 din Matricea1 este:");
				System.out.println();
				for (int i=0;i<randuri;i++) {
					for (int j=0;j<coloane;j++) {
						matriceRezultat[i][j]=matrice1[i][j]-matrice2[i][j];
						System.out.print(matriceRezultat[i][j]+" ");
						
						
					}
					System.out.println();
				}	
	}

}
