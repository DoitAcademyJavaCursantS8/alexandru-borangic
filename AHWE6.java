
public class AHWE6 {

	/*Write on the screen an application to compute:
		11223344 + 0.3 with 10 decimals
		1111 + 1111.22 without decimals
		1 + 0.1234 with 2 decimals
		String.format()  */
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub

				
		System.out.println(String.format("%.10f%n", (11223344 + 0.3)));
		System.out.println(String.format("%.0f%n", (1111 + 1111.22)));
		System.out.println(String.format("%.2f%n", (1 + 0.1234)));
	}

}
