import java.util.Scanner;

public class E3 {
	//Show the ASCII value of a character entered from
	//keyboard by user
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner in= new Scanner (System.in);
		System.out.println("Va rog inserati caracterul:");
		
		//By using the charAt function you are able to get the value
		//of the first char without using external casting.
		char caracter=in.next().charAt(0); 
		int ascii = (int) caracter;
		
        System.out.println("Caracterul in ASCII este: " + ascii); 
		
	}

}
