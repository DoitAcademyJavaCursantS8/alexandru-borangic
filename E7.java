import java.util.Scanner;

public class E7 {

	//Write a Java program that takes the user to provide a single
	//character from the alphabet. Print Vowel or Consonant, depending on
	//the user input. If the user input is not a letter (between a and z or A and
	//Z), or is a string of length > 1, print an error message.
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// introducem textul de la tastatura
		Scanner scan= new Scanner(System.in);
	   
	    
	    System.out.println("Va rog introduceti o litera:");
	    
	    String text= scan.nextLine();
	    
	    
	    //calculeaza lungimea sirului
	    int textLength = text.length();
	    
	    
	    if (textLength!=1) {
	    System.out.println("Lungimea sirului este mai mare de un caracter!");}
	    
	    //verifica daca este litera sau alt caracter
	    else if(!Character.isLetter(text.charAt(0))) {
	    	
	    	System.out.println("Nu ati introdus o litera!");}	
	    
	    
	   //daca este litera urmeaza pasii
	    else if (Character.isLetter(text.charAt(0))) {
	    	//transforma textul intr-un singur caracter
	    	 	char c = text.charAt(0);
	    	 	if ((c=='a'||c=='e'||c=='i'||c=='o'||c=='u'||c=='A'||c=='E'||c=='I'||
		    		c=='O'||c=='U')){
		    	
	    	 		System.out.println("Caracterul introdus este o vocala!");}
		    
		    else {
		    	
		    	System.out.println(" Caracterul introdus este o consoana!");}   
	    
	    
	    }
	    
	}

}
